# Tricks and tips by princebilly

## As a web developer choose your things wisely:

#### **fonts:**

*.woff: highly compressed loads very fast. woff2 format offers a 30% average compression gain over the , original woff and loads much faster than woff. But woff2 has no support in android 4.4 default browser, IE. woff is supported by all browsers now with compressions. So i prefer using:

```
@font-face {
font-family: 'MyWebFont';
src: url('myfont.woff2') format('woff2'),
url('myfont.woff') format('woff'),
url('myfont.ttf') format('truetype');
}
```

**front end:** tips: use gulp-ttf2woff and gulp-ttf2woff2. make two 3 separate path in font directory: woff2/, woff/, ttf/ 

#### **Image:**

*.jpeg for all photos(small file size), *.png(transparent photos), *.gif (animations).

tips: use gulp-imagemin(front-end) 

**back end: **use image compression libraries and force user to upload their photos in jpeg format or use image conversion library for you backend technology.

#### **Video & Audio:**

MP4(video) & MP3(audio)  (supported by all browser).

**front end:** use tools like winff (cross platform) (ffmpeg)

**back end: ** install ffmepg in your web-server if possible and evaluate different cmd from you backend technology to allow user uploading any format of videos as they don't have to be a geek like you to browse website. 

#### PDF:

Don't forget to compress pdf before adding it to your server as a super user. force user to upload small size PDF according to your need.  Depending only on computers for compressing pdf will be a bad idea.



## Cross platform :

**Conditional Comments**: Conditional comments allow you to link style sheets for different browsers, which is especially helpful when it comes to design challenges that are common with Internet Explorer

**Final Tests:** IE test(trident, tashman), edge(edge HTML), UC test (U3)

**Browser engines:** 

**Blink**:Opera, Google Chrome, Chromium, Vivaldi, Yandex ,

**Gecko:** Mozilla, Sea Monkey, Water fox

**Webkit**: safari, midori, android browser